import { useEffect, useState } from "react";
import useWeatherData from "../hooks/useWeatherData";
import WeatherGrid from "./WeatherGrid";

import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import WeatherChart from './weatherChart';
import Form from 'react-bootstrap/Form';
import './WeatherItem.scss';


const WeatherItem = (props) => {
    return (
        <Col className="weatherItem">
            <Button as="a" variant="flat" style={{width:'inherit'}} onClick={props.onClick} className='dayButton'>
                <div>{props.value.day}</div>
                <div>{props.value.date}</div>
                {/* it is displayed only on small screens (weatherItem.scss) */}
                <div>{props.value.dayShort}</div> 
            </Button>
        </Col>
    )
};


const WeatherList = () => {
    
    const [searchValue, setSearchValue] = useState('Zagreb');
    const { weatherData, isLoading } = useWeatherData(searchValue?`${searchValue}`:'');
    const [currentWeatherData] = weatherData; // take only first memeber of an array
    const dataDaily = [...new Map(weatherData.map(item =>[item['day'], item])).values()]; //return object with all properties unique by key
    const [weatherDataForSelectedDay, setWeatherDataForSelectedDay] = useState();
    
    function handleClick(day){
        setWeatherDataForSelectedDay(weatherData.filter(item => item.day === day))
    }

    function handleChange(event) {
        setSearchValue(event.target.value);
    }

    useEffect(() => {
            setWeatherDataForSelectedDay(weatherData.filter(item => item.day === currentWeatherData.day));
    }, [isLoading, weatherData, currentWeatherData]);

   
    return (
        <div>
            {!isLoading ? (
            <Container>
                <Row style={{'paddingBottom' : '2%'}}>
                    <Col >
                        <div className='mainInfo'>
                            <p>{weatherDataForSelectedDay && weatherDataForSelectedDay.length > 0 ? weatherDataForSelectedDay[0].city : ''} {weatherDataForSelectedDay && weatherDataForSelectedDay.length > 0 ? weatherDataForSelectedDay.country : ''}</p>
                            <p>{weatherDataForSelectedDay && weatherDataForSelectedDay.length > 0 ? weatherDataForSelectedDay[0].day : ''}</p>
                            <p>{weatherDataForSelectedDay && weatherDataForSelectedDay.length > 0 ? weatherDataForSelectedDay[0].dateLong : ''}</p>
                        </div>
                    </Col>
                    <Col>
                        <p style={{'textAlign': 'right'}}>City:</p>
                    </Col>
                    <Col >
                        <Form.Control
                            size="sm"
                            id="searchCity"
                            aria-describedby="search-input"
                            value={searchValue}
                            onChange={handleChange}
                        />
                        <Form.Text id="searchCity" muted>
                            Search city
                        </Form.Text>
                    </Col>
                </Row>
                <Row>
                    <WeatherGrid {...weatherDataForSelectedDay}/>
                </Row>
                <Row className="chart">
                    <WeatherChart {...weatherDataForSelectedDay}/>
                </Row>
                <Row>
                    {
                        dataDaily.map((itemData) => (<WeatherItem key={itemData.id} value={itemData}  onClick={() => handleClick(itemData.day)} />))
                    }        
                </Row>
            </Container>
             ) : (
                <p className="loading-text">Loading Data...</p>
              )}
        </div>
    )
};

export default WeatherList;
import Form from 'react-bootstrap/Form';

const CustomCheckbox = (props) => {
    const checkboxList = props.checkboxlist;
    return (
       
           <div>
                {checkboxList.map((option) => (
                    <Form.Check
                        inline
                        label={option.label}
                        name='group1'
                        type='radio'
                        id={`chk-${option.label}`}
                        key={option.id}
                        checked ={option.checked}
                        onChange={(e) => props.onChange(e,option)}
                    />
                ))}
          </div>
    )
    
}

export default CustomCheckbox;
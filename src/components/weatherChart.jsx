import { useEffect, useState } from 'react';
import CustomCheckbox from './CustomCheckbox';
import './WeatherChart.scss';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Filler,
  Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Filler,
  Legend
);

export default function WeatherChart(props) {
    
    const [chkList, setChkList] = useState([]);
    const weatherData = Object.values(props); 
    const labels = weatherData.map(item => item.time);
    const chkListItems = [
      {
        label: 'temperature',
        name: 'temperature',
        id: 1,
        checked: true
      },
      {
        label: 'humidity',
        name: 'humidity',
        id: 2,
        checked: false
      },
      {
        label: 'wind',
        name: 'wind',
        id: 3,
        checked: false
      },
      {
        label: 'pressure',
        name: 'pressure',
        id: 4,
        checked: false
      }
    ]

    function getDataset(chkList) {
      var dataset = [];
      chkList.map((chkItem) => {
          if (chkItem.checked === true && chkItem.label === 'temperature'){
            dataset.push({
              fill: false,
              label: chkItem.label,
              data: weatherData.map(item => item[chkItem.label]),
              borderColor: '#8BC34A',
              backgroundColor: '#8BC34A',
            },
            {
              fill: false,
              label: 'feels like',
              data: weatherData.map(item => item['feelsLike']),
              borderColor: '#CFFF8D',
              backgroundColor: '#CFFF8D',
            })
          }
          else if (chkItem.checked === true){
            dataset.push({
                fill: false,
                label: chkItem.label,
                data: weatherData.map(item => item[chkItem.label]),
                borderColor: '#8BC34A',
                backgroundColor: '#8BC34A',
              })
            }  
          });
        return dataset;
    }
    
    const data = {
        labels,
        datasets: getDataset(chkList)
      };

    const options = {
        responsive: true,
        plugins: {
          legend: {
            display: true,
            position: 'top',
          },
          title: {
            display: false,
            text: 'Weather Forecast',
          },
        },
      };

    const handleChange = (event, option) => {
      let chkListCopy = chkList.slice();
 
      chkListCopy.forEach((chkItem) => {
        if (chkItem === option){
          chkItem.checked = event.target.checked;
        }
        else {
          chkItem.checked = false;
        }
      });
      setChkList(chkListCopy);
    }

    useEffect(() => {
      setChkList(chkListItems);
    }, [])

    return (
      <div>
        <CustomCheckbox checkboxlist={chkList} onChange={handleChange}/>
        <Line options={options} data={data}/>
      </div>
    )
    
    
}


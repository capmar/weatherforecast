
import'./App.scss';
import WeatherList from "./components/weatherItem";
import ThemeProvider from 'react-bootstrap/ThemeProvider'

function App() {
  return (
    <ThemeProvider
      breakpoints={['xxxl', 'xxl', 'xl', 'lg', 'md', 'sm', 'xs', 'xxs']}
      minBreakpoint="xxs"
    >
      <div className="App">
        <h2 className="wonnaBeHeader" >Weather forecast</h2>
          <WeatherList />
      </div>
    </ThemeProvider>
  );
}

export default App;


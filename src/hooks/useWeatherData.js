import { useEffect, useState } from "react";


const useWeatherData = (searchCity) => {
    const [weatherData, setWeatherData] = useState([]);
    const [isLoading, setLoading] = useState(true);
    
    useEffect(() => {
        const fetchData = async () => {
            setLoading(true);
            fetch(
                `https://api.openweathermap.org/data/2.5/forecast?q=${searchCity},CRO&appid=7656917227e2d5923133f179261868ff&units=metric`
            )
            .then(res => res.json())
            .then((data) => {
                console.log(data);
                const preparedData = [];
                const city = data.city.name;
                const country = data.city.country;
                
                data.list.forEach(function (d, i){
                    const id = i;
                    const dateAndTime = d.dt_txt;
                    var moment = require('moment');
                    const day = moment(dateAndTime).format('dddd');
                    const dayShort = moment(dateAndTime).format('dd');
                    const date = moment(dateAndTime).format('DD/MM');
                    const dateLong = moment(dateAndTime).format('DD.MM.YYYY');
                    const time = moment(dateAndTime).format('LT');
                    const temperature = Math.round(d.main.temp);
                    const description = d.weather[0].description;
                    const icon = d.weather[0].icon;
                    const feelsLike =  Math.round(d.main.feels_like);
                    const humidity = d.main.humidity;
                    const pressure = d.main.pressure;
                    const wind = d.wind.speed;
                    const clouds = d.clouds.all;
    
                    preparedData.push({
                        id,
                        city,
                        country,
                        day,
                        dayShort,
                        date,
                        dateLong,
                        time,
                        temperature,
                        description,
                        icon,
                        feelsLike,
                        humidity,
                        pressure,
                        wind,
                        clouds
                    });
                });
               setWeatherData(preparedData);
            })
            .finally(() => setLoading(false));
        };

        const timer = setTimeout(() => { // gives user enough time to fully type in the search value
            fetchData();
        }, 500);
      
        return () => clearTimeout(timer);
    }, [searchCity])

    return { weatherData, isLoading}
};

export default useWeatherData;
